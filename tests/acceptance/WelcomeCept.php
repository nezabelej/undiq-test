<?php
    $I = new AcceptanceTester($scenario);
    $I->wantToTest("When the email address is invalid or valid.");
    $I->amOnPage('/enter_email');
    $I->fillField('exampleInputEmail1', 'notAnEmail');
    $I->click('Submit');
    $I->see('Invalid email');
    $I->cantSee('has been sent to');

    $I->amOnPage('/enter_email');
    $I->fillField('exampleInputEmail1', '');
    $I->click('Submit');
    $I->see('Invalid email');
    $I->cantSee('has been sent to');

    $I->amOnPage('/enter_email');
    $I->fillField('exampleInputEmail1', 'nezabelej@gmail.com');
    $I->click('Submit');
    $I->see('has been sent to');
    $I->cantSee('Invalid email');
?>
