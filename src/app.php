<?php
/**
 * Created by PhpStorm.
 * User: Neza
 * Date: 1.9.2016
 * Time: 22:49
 */

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

$routes->add('home', new Routing\Route('/', array(
    '_controller' => 'Controller\\EmailController::indexAction',
)));

$routes->add('email', new Routing\Route('/enter_email', array(
    '_controller' => 'Controller\\EmailController::indexAction',
)));

$routes->add('validate', new Routing\Route('/save_email', array(
    '_controller' => 'Controller\\EmailController::validateEmail',
)));


return $routes;

