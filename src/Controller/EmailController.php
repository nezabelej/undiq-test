<?php
/**
 * Created by PhpStorm.
 * User: Neza
 * Date: 2.9.2016
 * Time: 9:28
 */

namespace Controller;

use Mailgun\Mailgun;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EmailController
 * @package Email\Controller
 */

class EmailController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if (!(empty($request->getSession()->get('Error')))) {
            $error = $request->getSession()->get('Error');
            $request->getSession()->remove('Error');
            return new Response($this->render('email_form.html.twig', array('error' => $error)));
        }
        return new Response($this->render('email_form.html.twig', array()));

    }

    /**
     * @param Request $request
     * @return Response|static
     * @throws \Mailgun\Messages\Exceptions\MissingRequiredMIMEParameters
     */

    public function validateEmail(Request $request)
    {
        $email = $request->request->get('exampleInputEmail1');

        //validate email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response = RedirectResponse::create('enter_email');
            $request->getSession()->set('Error', 'Invalid email!');
            return $response;
        }

        //send mail
        $mgClient = new Mailgun('key-3b60f27707ae8af9439517976c57d04e');
        $domain = "nezabelej.mydomain.com";

        $result = $mgClient->sendMessage("$domain",
            array('from'    => 'Neža Belej <mailgun@nezabelej.mydomain.com>',
                'to'      => 'nezabelej@gmail.com',
                'subject' => 'Hello!',
                'text'    => $email.' just checked in on your page!'));

        //show next page
        return new Response($this->render('email_show.html.twig', array('email' => $email)));

    }
}