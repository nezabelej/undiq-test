<?php
/**
 * Created by PhpStorm.
 * User: Neza
 * Date: 2.9.2016
 * Time: 11:23
 */

namespace Controller;

use Twig_Loader_Filesystem;
use Twig_Environment;

class Controller
{
   public function render($name, $args)
    {
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../views');
        $twig = new Twig_Environment($loader, array());
        return $twig->render($name, $args);
    }
}